import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_builder/responsive_builder.dart';
import '/Constant/HappyShopColor.dart';
import 'login_cubit.dart';
import 'login_form.dart';


class LoginScaffold extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return WillPopScope(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Container(
            height: height,
            width: width,
            decoration: back(),
            child: Center(
              child: Column(
                children: <Widget>[
                  Container(
                    margin:const EdgeInsets.fromLTRB(0, 70, 0, 30),
                    child: Center(
                      child: new SvgPicture.network(
                        'https://smartkit.wrteam.in/smartkit/images/happyshopwhitelogo.svg',
                        width: 80.0,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Expanded(
                    child: ScreenTypeLayout(
                      mobile: Container(
                        width: double.infinity,
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(top: 10.0, bottom: 20),
                                child: Card(
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                  margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      welcomeHappyShopTxt(context),
                                       BlocProvider(
                                            create: (_) => LoginCubit(),
                                            child: LoginForm(),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }
  back() {
    return BoxDecoration(
      gradient: LinearGradient(begin: Alignment.topLeft, end: Alignment.bottomRight, colors: [primaryLight2, primaryLight3], stops: [0, 1]),
    );
  }

  welcomeHappyShopTxt(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 50.0, left: 30.0, right: 30.0),
        child: Align(
          alignment: Alignment.centerLeft,
          child: new Text(
            "Welcome to Mobifone Invoice",
            style: Theme.of(context).textTheme.headline6.copyWith(color: lightblack, fontWeight: FontWeight.bold),
          ),
        ));
  }
}
