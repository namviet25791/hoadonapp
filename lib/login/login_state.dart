import 'package:equatable/equatable.dart';
import 'package:flutter_bloc_article/models_validator/NotEmpty.dart';
import '../models_validator/email.dart';
import '../models_validator/password.dart';
import 'package:formz/formz.dart';

class LoginState extends Equatable {
  const LoginState({
    this.json = const<String, dynamic>{"username":"","password":""},
    this.jsonerror = const<String, String>{},
    this.status = FormzStatus.pure,
    this.exceptionError,
  });

  final Map<String, dynamic> json;
  final Map<String, dynamic> jsonerror;

  final FormzStatus status;
  final String exceptionError;

  @override
  List<Object> get props => [json,jsonerror, status, exceptionError];

  Map<String, dynamic> changeValue(Map<String, dynamic> items) {
    if(items != null) {
      Map<String, dynamic> json = {};
      json.addAll(this.json);
      json.addAll(items);
      return json;
    }
    return  this.json;
  }

  Map<String, dynamic> changeError(Map<String, dynamic> items) {
    if(items != null) {
      return items;
    }
    return this.jsonerror;
  }

  LoginState copyWith({
     Map<String, dynamic> json,
     Map<String, dynamic> jsonerror,
     FormzStatus status,
     String exceptionError,
  }) {

    return LoginState(
      json: changeValue(json),
      jsonerror: changeError(jsonerror),
      status: status ?? this.status,
      exceptionError: exceptionError ?? this.exceptionError,
    );
  }
}