import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_article/models_validator/NotEmpty.dart';
import 'package:formz/formz.dart';
import '../Constant/ApiString.dart';
import 'login_state.dart';
import 'package:http/http.dart' as http;

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(const LoginState());

  void valueChanged(Map<String, dynamic> items) {
    emit(state.copyWith(json: items));
  }

  Future<void> logInWithCredentials() async {
    NotEmpty username = NotEmpty.dirty(state.json["username"]?? '');
    NotEmpty password = NotEmpty.dirty(state.json["password"]?? '');
    NotEmpty masothue = NotEmpty.dirty(state.json["masothue"]?? '');
    FormzStatus status = Formz.validate([username,password,masothue]);

    if(status.isInvalid)
    {
      Map<String, dynamic> jsonerror = {"username":username.error,"password":password.error,"masothue":masothue.error};
      emit(state.copyWith(jsonerror: jsonerror,status: status));
    }
    else{
      emit(state.copyWith(jsonerror: {},status: FormzStatus.submissionInProgress));
      var client = http.Client();
      try {
        var url = Uri.parse(Until.GetBaseApi("TEST")+LoginAPI);
        var response = await client.post(url,
            body: state.json);
        var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as Map;
        var uri = Uri.parse(decodedResponse['uri'] as String);
        print(await client.get(uri));
      }
      on Exception catch (e) {
        print(e.toString());
      }
      finally {
        client.close();
      }
    }
  }
}