import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_article/common/TextInputForm.dart';
import '../common/Format.dart';
import 'login_cubit.dart';
import 'login_state.dart';
import 'package:formz/formz.dart';
import '../models_validator/password.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginCubit, LoginState>(
        listener: (context, state) {
          if (state.status.isSubmissionFailure) {
            print('submission failure');
          } else if (state.status.isSubmissionSuccess) {
            print('success');
          }
        },
        builder: (context, state) => Stack(
          children: [
              SingleChildScrollView(
                padding: const EdgeInsets.fromLTRB(38.0, 0, 38.0, 60),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      _MaSoThueInputField(),
                      _UserNameInputField(),
                      _PasswordInputField(),
                      _LoginButton(),
                    ],
                  ),
                ),
              ),
            state.status.isSubmissionInProgress
                ? Positioned(
              child: Align(
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
              ),
            ) : Container(),
          ],
        )
    );
  }
}

class _MaSoThueInputField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      buildWhen: (previous, current) => previous.jsonerror["masothue"]  != current.jsonerror["masothue"],
      builder: (context, state) {
        return TextInputForm(
          hint: 'Mã số thuế',
          key: const Key('masothue'),
          keyboardType: TextInputType.number,
          error: state.jsonerror["masothue"],
          formatter: UpperCaseTextFormatter(),
          onChanged: (value) => context.read<LoginCubit>().valueChanged({"masothue":value}),
        );
      },
    );
  }
}

class _UserNameInputField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      buildWhen: (previous, current) => previous.jsonerror["username"]  != current.jsonerror["username"],
      builder: (context, state) {
        return TextInputForm(
          hint: 'Tên đăng nhập',
          key: const Key('username'),

          keyboardType: TextInputType.text,
          error: state.jsonerror["username"],
          formatter: UpperCaseTextFormatter(),
          onChanged: (value) => context.read<LoginCubit>().valueChanged({"username":value}),
        );
      },
    );
  }
}

class _PasswordInputField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      buildWhen: (previous, current) => previous.jsonerror["password"]  != current.jsonerror["password"],
      builder: (context, state) {
        return TextInputForm(
          hint: 'Mật khẩu',
          isPasswordField: true,
          key: const Key('password'),
          keyboardType: TextInputType.text,
          error: state.jsonerror["password"],
          onChanged: (value) => context.read<LoginCubit>().valueChanged({"password":value}),
        );
      },
    );
  }
}

class _LoginButton extends StatelessWidget {
  const _LoginButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      builder: (context, state) {
        return Padding(
          padding: EdgeInsets.only(top: 20),
          child: CupertinoButton(
            padding: EdgeInsets.zero,
            child: Text('Login'),
            disabledColor: Colors.blueAccent.withOpacity(0.6),
            color: Colors.blueAccent,
            onPressed: () => context.read<LoginCubit>().logInWithCredentials()
          ),
        );
      },
    );
  }
}