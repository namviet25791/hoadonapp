import 'package:formz/formz.dart';


class NotEmpty extends FormzInput<String, String> {
  const NotEmpty.pure() : super.pure('');
  const NotEmpty.dirty([String value = '']) : super.dirty(value);


  @override
  String validator(String value) {
    if (value.isEmpty) {
      return "Không được bỏ trống";
    }
    return null;
  }
}
