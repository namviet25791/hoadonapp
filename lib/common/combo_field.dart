import 'package:flutter/material.dart';

import '../flutter_flow/flutter_flow_combo.dart';
import '../flutter_flow/flutter_flow_theme.dart';

class ComBoField extends StatelessWidget {
  final String hint;
  final ValueChanged<String> onChanged;
  final TextInputType keyboardType;
  final bool isPasswordField;
  final bool isRequiredField;
  final String error;
  final EdgeInsets padding;

  const ComBoField({
    Key key,
    this.hint = '',
    @required this.onChanged,
    @required this.keyboardType,
    this.isPasswordField = false,
    this.isRequiredField = false,
    this.error,
    this.padding = const EdgeInsets.all(0),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    OutlineInputBorder border = OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.grey, width: 0.0),
        borderRadius: BorderRadius.circular(10),
    );
    OutlineInputBorder errorBorder = OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.red, width: 0.0),
      borderRadius: BorderRadius.circular(10),
    );
    return Padding(
      padding: EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
      child: FlutterFlowDropDown(
        options: ['Team 1', 'Team 2', 'Team 3'],
        /*onChanged: (val) => setState(() => teamSelectValue = val),*/
        width: double.infinity,
        height: 60,
        textStyle: FlutterFlowTheme.of(context).bodyText1,
        icon: Icon(
          Icons.keyboard_arrow_down_rounded,
          color: FlutterFlowTheme.of(context).secondaryText,
          size: 15,
        ),
        fillColor: FlutterFlowTheme.of(context).secondaryBackground,
        elevation: 2,
        borderColor: FlutterFlowTheme.of(context).primaryBackground,
        borderWidth: 2,
        borderRadius: 8,
        margin: EdgeInsetsDirectional.fromSTEB(24, 4, 12, 4),
      ),
    );
  }
}
