import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextInputForm extends StatelessWidget {
  final String hint;
  final ValueChanged<String> onChanged;
  final TextInputType keyboardType;
  final bool isPasswordField;
  final bool isRequiredField;
  final String error;
  final TextInputFormatter formatter;

  const TextInputForm({
    Key key,
    this.hint = '',
    @required this.onChanged,
    @required this.keyboardType,
    this.isPasswordField = false,
    this.isRequiredField = false,
    this.error,
    this.formatter
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    OutlineInputBorder border = OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.grey, width: 0.0),
        borderRadius: BorderRadius.circular(10),
    );
    OutlineInputBorder errorBorder = OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.red, width: 0.0),
      borderRadius: BorderRadius.circular(10),
    );
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              hint,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 13,
              ),
            ),
            TextFormField(
              inputFormatters: [
                formatter,
              ],
              keyboardType: keyboardType,
              decoration: InputDecoration(
                fillColor: Colors.white,
                contentPadding: EdgeInsets.symmetric(horizontal: 12),
                filled: true,
                border: border,
                disabledBorder: border,
                enabledBorder: border,
                errorBorder: errorBorder,
                focusedErrorBorder: errorBorder,
                errorText: error,
                floatingLabelBehavior: FloatingLabelBehavior.never,
              ),
              autocorrect: false,
              textInputAction: TextInputAction.done,
              maxLines: 1,
              onChanged: onChanged,
            ),
          ],
        ),
      ),
    );
  }
}
