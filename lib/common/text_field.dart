import 'package:flutter/material.dart';

import '../flutter_flow/flutter_flow_theme.dart';

class TextField extends StatelessWidget {
  final String hint;
  final ValueChanged<String> onChanged;
  final TextInputType keyboardType;
  final bool isPasswordField;
  final bool isRequiredField;
  final String error;
  final EdgeInsets padding;

  const TextField({
    Key key,
    this.hint = '',
    @required this.onChanged,
    @required this.keyboardType,
    this.isPasswordField = false,
    this.isRequiredField = false,
    this.error,
    this.padding = const EdgeInsets.all(0),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    OutlineInputBorder border = OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.grey, width: 0.0),
        borderRadius: BorderRadius.circular(10),
    );
    OutlineInputBorder errorBorder = OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.red, width: 0.0),
      borderRadius: BorderRadius.circular(10),
    );
    return Padding(
      padding: EdgeInsetsDirectional.fromSTEB(16, 16, 16, 0),
      child: TextFormField(
        obscureText: false,
        decoration: InputDecoration(
          labelText: 'Task Name',
          labelStyle: FlutterFlowTheme.of(context).title3.override(
            fontFamily: 'Poppins',
            color: FlutterFlowTheme.of(context).secondaryText,
            fontWeight: FontWeight.normal,
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: FlutterFlowTheme.of(context).primaryBackground,
              width: 2,
            ),
            borderRadius: BorderRadius.circular(0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: FlutterFlowTheme.of(context).primaryBackground,
              width: 2,
            ),
            borderRadius: BorderRadius.circular(0),
          ),
          contentPadding: EdgeInsetsDirectional.fromSTEB(20, 32, 20, 12),
        ),
        style: FlutterFlowTheme.of(context).title3.override(
          fontFamily: 'Poppins',
          fontSize: 12,
        ),
        textAlign: TextAlign.start,
        maxLines: 1,
      ),
    );
  }
}
