import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_article/home/home_state.dart';
import 'package:flutter_svg/svg.dart';
import '../Constant/HappyShopColor.dart';
import 'home_cubit.dart';


class HomeScaffold extends StatelessWidget {
  BuildContext _context;
  @override
  Widget build(BuildContext context) {
    _context = context;
    return BlocConsumer<HomeCubit, HomeState>(
        builder: (context, state) => Scaffold(
              appBar: AppBar(
                elevation: 0,
                title: _title(),
              ),
              bottomNavigationBar: _bottomNav(),
              body: _body(),
            ));
  }
}
class _bottomNav extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
        buildWhen: (previous, current) =>
            previous.curBottom != current.curBottom,
        builder: (context, state) {
          return BottomNavigationBar(
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: SvgPicture.network(
                    'https://smartkit.wrteam.in/smartkit/grobag/home.svg',
                  ),
                  activeIcon: SvgPicture.network(
                    'https://smartkit.wrteam.in/smartkit/grobag/active-home.svg',
                  ),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.network(
                      'https://smartkit.wrteam.in/smartkit/grobag/category.svg'),
                  activeIcon: SvgPicture.network(
                    'https://smartkit.wrteam.in/smartkit/grobag/active-category.svg',
                  ),
                  label: 'Category',
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.network(
                      'https://smartkit.wrteam.in/smartkit/grobag/favorite.svg'),
                  activeIcon: SvgPicture.network(
                    'https://smartkit.wrteam.in/smartkit/grobag/active-favorite.svg',
                  ),
                  label: 'Favorite',
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.network(
                      'https://smartkit.wrteam.in/smartkit/grobag/profile.svg'),
                  activeIcon: SvgPicture.network(
                    'https://smartkit.wrteam.in/smartkit/grobag/active-profile.svg',
                  ),
                  label: 'Profile',
                ),
              ],
              type: BottomNavigationBarType.fixed,
              currentIndex: state.curBottom,
              selectedItemColor: primary,
              onTap: (int index) {
                context.read<HomeCubit>().curBottomChanged(index);
              },
              elevation: 25);
        });
  }
}
class _title extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
        buildWhen: (previous, current) =>
        previous.curBottom != current.curBottom,
        builder: (context, state) {
          return Text("test");
        });
  }
}
class _body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
        buildWhen: (previous, current) =>
        previous.curBottom != current.curBottom,
        builder: (context, state) {
          return Text("test");
        });
  }
}
