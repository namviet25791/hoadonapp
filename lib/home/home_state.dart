import 'package:equatable/equatable.dart';
import 'package:flutter_bloc_article/models_validator/NotEmpty.dart';
import '../models_validator/email.dart';
import '../models_validator/password.dart';
import 'package:formz/formz.dart';

class HomeState extends Equatable {
  const HomeState({
    this.curBottom = 0,
  });

  final int curBottom;

  @override
  List<Object> get props => [int];

  HomeState copyWith({
    int curBottom,
  }) {
    return HomeState(
      curBottom: curBottom ?? this.curBottom,
    );
  }
}